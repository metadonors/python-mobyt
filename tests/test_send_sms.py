import os
import logging

from pymobyt import Mobyt

MOBYT_USERNAME = os.getenv('MOBYT_USERNAME', False)
MOBYT_PASSWORD = os.getenv('MOBYT_PASSWORD', False)

MOBYT_SENDER = os.getenv('MOBYT_SENDER', False)
MOBYT_DESTINATION = os.getenv('MOBYT_DESTINATION', False)

class TestSMS:
    def test_send_sms(self):
        assert MOBYT_USERNAME != False
        assert MOBYT_PASSWORD != False
        assert MOBYT_SENDER != False
        assert MOBYT_DESTINATION != False


        mobyt = Mobyt(MOBYT_USERNAME, MOBYT_PASSWORD)

        data = {
            'message': "Test SMS",
            'recipients': [MOBYT_DESTINATION],
        }

        ret = mobyt.send_sms(**data)

        assert ret['result'] == 'OK'